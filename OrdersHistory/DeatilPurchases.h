//
//  DeatilPurchases.h
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface DeatilPurchases : UIView
{
    IBOutlet UIView *viewContentS;
    
}

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewContent;
//Primera vista completa
@property (strong, nonatomic) IBOutlet UIView *viewPaidOutContent;

//Segunda vista completa
@property (strong, nonatomic) IBOutlet UIView *viewAssortmentContent;
//Vista con iconos que cambian de color
@property (strong, nonatomic) IBOutlet UIView *viewStatusContent;
//vista de detalle de compra
@property (strong, nonatomic) IBOutlet UIView *viewDetailContent;
//Vista de ultima recarga
@property (strong, nonatomic) IBOutlet UIView *viewLastRefillContent;
//vista de total
@property (strong, nonatomic) IBOutlet UIView *viewTotalContent;


//Elementos de la primera vista
@property (strong, nonatomic) IBOutlet UIImageView *iconCoinColor;
@property (strong, nonatomic) IBOutlet UIView *viewBlank;
@property (strong, nonatomic) IBOutlet UIView *viewForIcons;
@property (strong, nonatomic) IBOutlet UIView *viewForDetails;
@property (strong, nonatomic) IBOutlet UIView *viewForLastRefill;
@property (strong, nonatomic) IBOutlet UIView *viewForTotal;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeighLastRefill;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeighSection;
- (IBAction)btnNext:(id)sender;



//Elementos de la primer vista
@property (strong, nonatomic) IBOutlet UILabel *labelTittleC1;
@property (strong, nonatomic) IBOutlet UILabel *labelDateC1;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewIconCoin;
@property (strong, nonatomic) IBOutlet UILabel *labelStatusCoin;
@property (strong, nonatomic) IBOutlet UILabel *labelDateStatusCoin;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewIconTicket;
@property (strong, nonatomic) IBOutlet UILabel *labelStatusTicket;
@property (strong, nonatomic) IBOutlet UILabel *labelDateStatusTicket;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewIconBus;
@property (strong, nonatomic) IBOutlet UILabel *labelStatusBus;
@property (strong, nonatomic) IBOutlet UILabel *labelDateStatusBus;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewIconTank;
@property (strong, nonatomic) IBOutlet UILabel *labelStatusTank;
@property (strong, nonatomic) IBOutlet UILabel *labelDateStatusTank;


//Elementos de la segunda vista
@property (strong, nonatomic) IBOutlet UILabel *labelTittleC2;
@property (strong, nonatomic) IBOutlet UILabel *labelDateC2;
@property (strong, nonatomic) IBOutlet UILabel *labelAddressRefill;
@property (strong, nonatomic) IBOutlet UILabel *labelAddressRefillValue;
@property (strong, nonatomic) IBOutlet UILabel *labelDateRefill;
@property (strong, nonatomic) IBOutlet UILabel *labelDateRefillValue;
@property (strong, nonatomic) IBOutlet UILabel *labelPaymentMethod;
@property (strong, nonatomic) IBOutlet UILabel *labelPaymentMethodValue;
@property (strong, nonatomic) IBOutlet UILabel *labelNumberCard;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewIconBrandCard;
@property (strong, nonatomic) IBOutlet UILabel *labelNumberCardValue;

//Elementos de la tercera vista
@property (strong, nonatomic) IBOutlet UILabel *labelBeforeRefill;
@property (strong, nonatomic) IBOutlet UILabel *labelAfterRefill;
@property (strong, nonatomic) IBOutlet UILabel *labelPorcBefore;
@property (strong, nonatomic) IBOutlet UILabel *labelPorcAfter;


//Elementos de la cuarta vista
@property (strong, nonatomic) IBOutlet UILabel *labelTankName;
@property (strong, nonatomic) IBOutlet UILabel *labelTotal;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalValue;

@end

