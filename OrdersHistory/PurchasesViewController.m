//
//  PurchasesViewController.m
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import "PurchasesViewController.h"

@interface PurchasesViewController ()

@end

@implementation PurchasesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initConfig];
}
- (void)viewDidLayoutSubviews
{
//    [self createSubview];
}

/**
 Inicia las configuraciones inicales del ViewController
 */
- (void)initConfig
{
    //                                                      //El titulo del back.
    self.navigationController.navigationBar.topItem.title = @"";
    //                                                      //Titulo del navigation bar.
    self.navigationItem.title = @"Detalle del pedido";
    
     [self createSubview];
}

- (void)createSubview
{
    CGRect frame;
    frame.size.width = scrollView.frame.size.width;
    frame.size.height = 950;
    frame.origin.x = scrollView.frame.origin.x;
    frame.origin.y = scrollView.frame.origin.y;
    
    auxDeatailPurchase = [[DeatilPurchases alloc] initWithFrame:frame];
//    auxDeatailPurchase.viewPaidOutContent.backgroundColor = [UIColor redColor];
    

//    [auxDeatailPurchase.viewStatusContent layoutIfNeeded];
//    auxDeatailPurchase.viewStatusContent.frame = CGRectMake(0, 0,CGRectGetWidth(auxDeatailPurchase.viewStatus.bounds), CGRectGetHeight(auxDeatailPurchase.viewStatus.bounds));

    NSLog(@"Frame auxDeatailPurchase.viewStatusContent: %@", NSStringFromCGRect(auxDeatailPurchase.viewStatusContent.bounds));
    NSLog(@"Frame auxDeatailPurchase.viewStatusContent: %@", NSStringFromCGRect(auxDeatailPurchase.viewStatusContent.frame));
//    NSLog(@"Frame auxDeatailPurchase.viewStatus: %@", NSStringFromCGRect(auxDeatailPurchase.viewStatus.bounds));
//    NSLog(@"Frame auxDeatailPurchase.viewStatus: %@", NSStringFromCGRect(auxDeatailPurchase.viewStatus.frame));
//
    scrollView.contentSize = CGSizeMake(0, auxDeatailPurchase.viewAssortmentContent.frame.size.height);//scrollView.frame.size.height);
    [scrollView addSubview:auxDeatailPurchase];
//
//    [self roundCornerAndShawdow:[auxDeatailPurchase.viewContent objectAtIndex:1]];
}

- (void)roundCornerAndShawdow:(UIView *)viewCard
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:viewCard.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];

    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = viewCard.bounds;
    maskLayer.path  = maskPath.CGPath;
    maskLayer.shadowColor = [UIColor grayColor].CGColor;
    maskLayer.shadowOpacity = 1.0;
    maskLayer.shadowRadius = 5.0;
    maskLayer.shadowOffset = CGSizeMake(2, 2);

    viewCard.backgroundColor = [UIColor whiteColor];
    viewCard.layer.cornerRadius = 10.0;
    viewCard.layer.shadowColor = [UIColor grayColor].CGColor;
    viewCard.layer.shadowOpacity = 1.0;
    viewCard.layer.shadowRadius = 3.0;
    viewCard.layer.shadowOffset = CGSizeMake(0 , 2);//CGSizeMake(2, 2);
    viewCard.layer.masksToBounds = NO;
    

}

- (void)roundCornerButton
{
    
}


@end
