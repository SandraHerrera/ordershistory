//
//  main.m
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
