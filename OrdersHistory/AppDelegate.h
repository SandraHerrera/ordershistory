//
//  AppDelegate.h
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

