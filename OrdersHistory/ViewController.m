//
//  ViewController.m
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSArray *arrayOptions;// @{@"Pedido Pagado", @"Pedido Confirmado", @"Pedido en camino", @"Pedido Surtido"};
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initDataTable];
    
}

- (void)initDataTable
{
    arrayOptions = [NSArray arrayWithObjects:@"Pedido Pagado", @"Pedido Confirmado", @"Pedido en camino", @"Pedido Surtido", nil];
}

/**
 Manda al detalle de la compra
 */
- (void)gotToPurchasesViewController
{
    PurchasesViewController *viewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"storyboardPurchases"];
    [self.navigationController pushViewController:viewController animated:YES];
}



- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayOptions.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.textLabel.text = [arrayOptions objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self gotToPurchasesViewController];
}





@end
