//
//  ViewController.h
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PurchasesViewController.h"

@interface ViewController : UIViewController
<
UITableViewDelegate,
UITableViewDataSource
>
{
    
    IBOutlet UITableView *tableView;
}

@end

