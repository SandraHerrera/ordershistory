//
//  DeatilPurchases.m
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import "DeatilPurchases.h"

@implementation DeatilPurchases

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self customInit];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self customInit];
    }
    
    return self;
}

/**
 Agregar todas las subvistas
 */
-(void)customInit
{
    [[NSBundle mainBundle] loadNibNamed:@"DeatilPurchasesView" owner:self options:nil];
    [self addSubview:_viewBlank];
    [_viewForIcons addSubview:_viewStatusContent];
    [_viewForDetails addSubview:_viewDetailContent];
    [_viewForLastRefill addSubview:_viewLastRefillContent];
    [_viewForTotal addSubview:_viewTotalContent];
}


/**
 Acción del botón

 @param sender <#sender description#>
 */
- (IBAction)btnNext:(id)sender
{
    _viewLastRefillContent.hidden=YES;
    _constraintHeighSection.constant = 0.f;
    _constraintHeighLastRefill.constant = 0.f;
}
@end
