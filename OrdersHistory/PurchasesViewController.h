//
//  PurchasesViewController.h
//  OrdersHistory
//
//  Created by Gerardo Zamudio on 11/6/18.
//  Copyright © 2018 Gerardo Zamudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeatilPurchases.h"

NS_ASSUME_NONNULL_BEGIN

@interface PurchasesViewController : UIViewController
{
    IBOutlet UIScrollView *scrollView;
    
    DeatilPurchases *auxDeatailPurchase;
}
@end

NS_ASSUME_NONNULL_END
